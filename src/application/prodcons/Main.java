package application.prodcons;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Main {

	public static final int MAX_SIZE = 5;

	public static void main(String[] args) {
		new Main().start();
	}

	private void start() {
		List<IProduct> list = new ArrayList<IProduct>();
		
		Producer producer = new Producer(list);
		Consumer consumer = new Consumer(list);
		
		producer.start();
		consumer.start();
	}
	
	private void startSemaphores() {
		List<IProduct> list = new ArrayList<IProduct>();
		
		Semaphore semCons = new Semaphore(0);
		Semaphore semProd = new Semaphore(MAX_SIZE);
		ProducerSemaphore producer = new ProducerSemaphore(semCons, semProd, list);
		ConsumerSemaphore consumer = new ConsumerSemaphore(semCons, semProd, list);
		
		producer.start();
		consumer.start();
	}

}
