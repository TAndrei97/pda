package application.prodcons;

import java.util.List;

public class Producer extends Thread {

	List<IProduct> list;

	public Producer(List<IProduct> list) {
		super();
		this.list = list;
	}

	@Override
	public void run() {
		while(true) {
			IProduct product = new Product();
			synchronized (list) {
				while(list.size() == Main.MAX_SIZE) {
					try {
						list.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				list.add(product);
				list.notifyAll();
			}
		}
	}

	
	
}
