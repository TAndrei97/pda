package application.prodcons;

import java.util.List;

public class Consumer extends Thread{

	private List<IProduct> list;

	public Consumer(List<IProduct> list) {
		super();
		this.list = list;
	}

	@Override
	public void run() {
		while(true) {
			IProduct product;
			synchronized (list) {
				while (list.isEmpty()) {
					try {
						list.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				product = list.remove(0);
				list.notifyAll();
			}
			product.consume();
		}
	}
	
	

}
