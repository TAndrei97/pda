package application.prodcons;

import java.util.List;
import java.util.concurrent.Semaphore;

public class ProducerSemaphore extends Thread{

	private Semaphore semProd;
	private Semaphore semCons;
	private List<IProduct> list;

	
	
	public ProducerSemaphore(Semaphore semCons, Semaphore semProd, List<IProduct> list) {
		super();
		this.semCons = semCons;
		this.semProd = semProd;
		this.list = list;
	}



	@Override
	public void run() {
		while(true) {
			try {
				semProd.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			IProduct product = new Product();
			synchronized (list) {
				list.add(product);
			}
			semCons.release();
		}
	}
}
