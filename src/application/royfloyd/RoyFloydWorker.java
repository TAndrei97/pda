package application.royfloyd;

public class RoyFloydWorker extends Thread {

	private int matrix[][];
	private int workerNumber;
	
	public RoyFloydWorker(int[][] matrix, int workerNumber) {
		super();
		this.matrix = matrix;
		this.workerNumber = workerNumber;
	}

	@Override
	public void run() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				synchronized (matrix) {
					if (matrix[i][workerNumber] != Integer.MAX_VALUE && 
							matrix[workerNumber][j] != Integer.MAX_VALUE && 
							matrix[i][j] > matrix[i][workerNumber] + matrix[workerNumber][j]) {
						matrix[i][j] = matrix[i][workerNumber] + matrix[workerNumber][j];
					}
				}
			}
		}
	}

}
