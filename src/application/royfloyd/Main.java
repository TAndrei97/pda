package application.royfloyd;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		new Main().start();
	}

	private List<RoyFloydWorker> threads;

	private void start() {
		threads = new ArrayList<RoyFloydWorker>();
		int matrix[][] = {{0, 2, Integer.MAX_VALUE, 10, Integer.MAX_VALUE}, 
				{2, 0, 3, Integer.MAX_VALUE, Integer.MAX_VALUE}, 
				{Integer.MAX_VALUE, 3, 0, 1, 8},
				{10, Integer.MAX_VALUE, 1, 0, Integer.MAX_VALUE}, 
				{Integer.MAX_VALUE, Integer.MAX_VALUE, 8, Integer.MAX_VALUE, 0}};
		print(matrix);
		for(int i = 0; i < matrix.length; i++) {
			RoyFloydWorker thread = new RoyFloydWorker(matrix, i);
			threads.add(thread);
		}
		
		for(RoyFloydWorker thread : threads) {
			thread.start();
		}

		for(RoyFloydWorker thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		print(matrix);
	}

	private void print(int[][] matrix) {
		for(int i = 0; i < matrix.length; i++) {
			for(int j = 0; j < matrix.length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}

}
